module A exposing (..)

{-| Test module.

@docs puts
-}

{-| A test.
-}
puts : String -> String
puts a = a
